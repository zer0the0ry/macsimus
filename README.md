# What is macsimus?

Macsimus is a bash shell script tool to quickly randomize your mac address.

![](https://zer0.life/2020-04-24_19-11.png)

----

## Requirements

* Linux
* sudo privileges on the system.

## How to use

From the command line change directory to the file macsimus.sh 

```
sudo bash macsimus.sh
```

You can also make the file executable like this.

```
chmod +x macsimus.sh
```

Now it can be run with this command.

```
sudo ./macsimus.sh
```

t

